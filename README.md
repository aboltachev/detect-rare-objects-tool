# DRObj  Tool
DRObj is a object detection tool, which uses Meta-Learning to detect rare objects.
This tool is an implementation of the rare object detection approach described in [this article](https://openaccess.thecvf.com/content_ICCV_2019/papers/Wang_Meta-Learning_to_Detect_Rare_Objects_ICCV_2019_paper.pdf).

DRObj is implemented as a client-server application.<br/>
DRObj uses PyTorch as a main deep learning engine.<br/>
Python and qRPC used to write server with DL backend.<br/>
C++/QT as a framework for GUI client application.<br/>

# Key Features
## DL backend
 * Train models on YOLOv2 and Faster R-CNN architectures to recognize new classes based on several examples (*Meta-Learning approach*)
 * Fine-tuning the model on new data without using *Meta-Learning approach*
 * Using trained models to object detection
## GUI Client app
 * Creating a project with training and test datasets
 * Keeping the current history of project changes
 * Sending data to DL server for model training and testing
 * (**Maybe**)Export trained model from server

# Requirements
 * Server:
    1. Python 3.6 or above
    2. PyTorch 1.6.0 or above
    3. gRPC and ProtoBuff 3
 * GUI Client:
    1. QT
    2. CMake

# Installation
## TBD

# Server API
## TBD

# Tutorials
## TBD
